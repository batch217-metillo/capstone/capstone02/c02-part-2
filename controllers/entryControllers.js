/*
	Entry Point for Registration and Authentication
*/

const User = require("../models/userModel.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// User Registration
/*
@desc User cannot register using the same email address.
@param {JSON Object}
@return {String} different messages
*/
module.exports.userRegistration = (reqBody, res) => {
	return User.findOne({emailAddress: reqBody.emailAddress}).then(result => {
		if(result !=null && result.emailAddress == reqBody.emailAddress) {
			return `Duplicate email found. 

			Please use another email to register, ${reqBody.firstName}.`
		}else {
			let newUser = new User({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				emailAddress: reqBody.emailAddress,
				phoneNumber: reqBody.phoneNumber,
				password: bcrypt.hashSync(reqBody.password, 10)
			});

			return newUser.save().then((saveUser, saveErr) => {
				if(saveErr) {
					return false;
				}else {
					return `User has been registered!

						Hello, ${reqBody.firstName}!`
				}
			})
		}
	})
}

// User Login
/*
@desc User cannot login using incorrect email address or password
@param {JSON Object} - email address and password
@return {String} different messages
*/
module.exports.userLogin = (reqBody, res) => {
	return User.findOne({emailAddress: reqBody.emailAddress}).then(result => {
		if(result == null){
			return `Email address does not exist`;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				// Generate an access
				return {access: auth.createAccessToken(result)}
			} else {
				return `Password is incorrect`
			}
		}
	})
}