const User = require("../models/userModel.js");
const Product = require("../models/productModel.js");
const auth = require("../auth.js");

// Create Product
/*
@desc This is for admin role only. Non-Admin/User cannot add products on the database.
@param {JSON Object}
@return {String} different messages
*/
module.exports.addProduct = (data) => {
	if(data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
			category: data.product.category,
			description: data.product.description,
			stocks: data.product.stocks,
			price: data.product.price
		})

		return newProduct.save().then((newProduct, error) => {
			if(error) {
				return false
			}else {
				return `Product has been added!`
			}
		})
	}else {

		let message = Promise.resolve(`You are not authorized to perform this task.

			You must be an ADMIN to access this.`)
		return message.then((value) => {
			return value
		})
	}
}

// Get All Products (active or inactive)
/*
@desc This is for admin role only. Admin role can view all products, active or inactive. Non-Admin/User cannot view inactive products on the database.
@param {JSON Object}
@return {String} different messages
*/
module.exports.getAllProducts = (data) => {
	if(data.isAdmin){
		return Product.find({}).then(result => {
		return result
		})
	}else {

		let message = Promise.resolve(`You are not authorized to perform this task.

		You must be an ADMIN to access this.`)
		return message.then((value) => {
			return value
		})
	}
}

// Get All ACTIVE Product 
/*
@desc All Users (Admin and Non-Admin Role) can view all active products on the database.
@param {JSON Object}
@return {String} different messages
*/
module.exports.getAllActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	})
}

// Get Single Product (productId)
/*
@desc All Users (Admin and Non-Admin) can view a specific product (active or inactive) using its id
@param {JSON Object}
@return {String} different messages
*/
module.exports.getSpecificProduct = (productId) => {
	return Product.findById(productId).then(result => {
		return result
	})
}

// Below codes are not working
// Get Single Product (productName)
/*
@desc All Users (Admin and Non-Admin) can view a specific product (active or inactive) using its name
@param {JSON Object}
@return {String} different messages
*/
/*module.exports.getSpecificProductUsingProductName = (productName) => {
	return Product.find({productName}).then(result => {
		return result
	})
}*/