const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product name is required!"]
	},
	category: {
		type: String,
		required: [true, "Product category is required!"]
	},
	description: {
		type: String,
		required: [true, "Product description is required!"]
	},
	stocks: {
		type: Number,
		required: [true, "Stock number is required!"]
	},
	price: {
		type: Number,
		required: [true, "Product price is required!"]
	},
	isActive: {
		type: Boolean,
		default: true,
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [{
		orderId: {
			type: String
		}
	}]
})

module.exports = mongoose.model("Product", productSchema)