const jwt = require("jsonwebtoken");
const secret = "ECommerceAPI";

// Create a token using the jsonwebtoken package from NPM
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		emailAddress: user.emailAddress,
		password: user.password,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
}

// Verify a token
module.exports.verify =(req, res, next) => {
	let token = req.headers.authorization;
	if (typeof token !== undefined) {
		console.log(token)

		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return res.send({
					auth: "Invalid Token"
				})
			} else {
				next()
			}
		})
	} else {
		res.send(`No token provided`)
	}
}

// Decode user details from the token
module.exports.decode = (token) => {
	if(typeof token !== "undefined") {
		
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			if(err) {
				return null
			}else {
				return jwt.decode(token, {complete: true}).payload
			}
		})
	}else {
		return null
	}
}